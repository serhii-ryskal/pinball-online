var peerConnectionConfig = {
  'iceServers': [
    {'urls': 'stun:stun.services.mozilla.com'},
    {'urls': 'stun:stun.l.google.com:19302'}
  ]
};

let localVideo = document.getElementById('localVideo');
let peerConnection;
let serverConnection;
let controlChannel;
let uuid = `${(~~(Math.random()*1e8)).toString(16)}${(~~(Math.random()*1e8)).toString(16)}` +
  `${(~~(Math.random()*1e8)).toString(16)}${(~~(Math.random()*1e8)).toString(16)}`;
serverConnection = new WebSocket('wss://' + window.location.hostname + ':8443');
serverConnection.onmessage = gotMessageFromServer;

window.addEventListener("keydown", handleKeyEvent);
window.addEventListener("keyup", handleKeyEvent);

function clientConnect() {
  peerConnection = new RTCPeerConnection(peerConnectionConfig);
  peerConnection.onicecandidate = gotIceCandidate;
  peerConnection.ontrack = gotRemoteStream;
  peerConnection.createOffer({'offerToReceiveAudio':true,'offerToReceiveVideo':true}).then(createdDescription).catch(errorHandler);

  peerConnection.addEventListener('signalingstatechange', () => console.log('Signaling state ' + peerConnection.signalingState));
  peerConnection.addEventListener('icegatheringstatechange', () => console.log('ICE gathering state ' + peerConnection.iceGatheringState));
  peerConnection.addEventListener('connectionstatechange', () => console.log('Connection state ' + peerConnection.connectionState));

  controlChannel = peerConnection.createDataChannel("controlChannel");
}

function gotRemoteStream(event) {
  console.log('got remote stream');
  localVideo.srcObject = event.streams[0];
}

function gotIceCandidate(event) {
  if(event.candidate != null) {
    serverConnection.send(JSON.stringify({'ice': event.candidate, 'uuid': uuid}));
  }
}

function errorHandler(error) {
  console.log(error);
}

function createdDescription(description) {
  peerConnection.setLocalDescription(description).then(function() {
    serverConnection.send(JSON.stringify({'sdp': peerConnection.localDescription, 'uuid': uuid}));
  }).catch(errorHandler);
}

function gotMessageFromServer(message) {
  var signal = JSON.parse(message.data);
  if(signal.uuid === uuid) return;

  if(signal.sdp) {
    peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp)).then(function() {
      if(signal.sdp.type === 'offer') {
        peerConnection.createAnswer().then(createdDescription).catch(errorHandler);
      }
    }).catch(errorHandler);
  } else if(signal.ice) {
    peerConnection.addIceCandidate(new RTCIceCandidate(signal.ice)).catch(errorHandler);
  }
}

function handleKeyEvent(e) {
  if(controlChannel && controlChannel.readyState === "open") {
    // "/" - right flipper      - 191
    // "x" - left table bump    - 88
    // "." - right table bump   - 190
    // "UP" - bottom table bump - 38
    // "Space" - launcher       - 32
    const keyCode = e.keyCode;
    if(keyCode === 191 || keyCode === 88 || keyCode === 190 || keyCode === 38 || keyCode === 32 || keyCode === 90) {
      e.preventDefault();
      e.stopPropagation();
      controlChannel.send(JSON.stringify({
        type: e.type,
        char: e.char,
        key: e.key,
        keyCode: e.keyCode
      }));
    }
  }
}