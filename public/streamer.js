var peerConnectionConfig = {
  'iceServers': [
    {'urls': 'stun:stun.l.google.com:19302'}
  ]
};

var peerMap = {};
var serverConnection;
var receiveChannel;
var stream;
var uuid = `${(~~(Math.random()*1e8)).toString(16)}${(~~(Math.random()*1e8)).toString(16)}` +
  `${(~~(Math.random()*1e8)).toString(16)}${(~~(Math.random()*1e8)).toString(16)}`;

function pageReady() {
  serverConnection = new WebSocket('wss://' + window.location.hostname + ':8443');
  serverConnection.onmessage = gotMessageFromServer;
  // Setup emulator
  Dos(document.getElementById("jsdos")).ready((fs, main) => {
    fs.extract("win9x-3d-pinball-space-cadet.zip").then(() => {
      main(["-c", "AUTORUN.BAT"]);
      initServer();
    });
  });
}

function initServer() {
  var canvas = document.getElementById("jsdos");
  stream = canvas.captureStream(60);
}

function gotIceCandidate(event) {
  if(event.candidate != null) {
    serverConnection.send(JSON.stringify({'ice': event.candidate, 'uuid': uuid}));
  }
}

function receiveChannelCallback(event) {
  event.channel.onmessage = e => {
    const keyEvent = JSON.parse(e.data);
    document.getElementById("jsdos").dispatchEvent(new KeyboardEvent(keyEvent.type, {
      bubbles: true,
      cancelable: true,
      char: keyEvent.char,
      key: keyEvent.key,
      keyCode: keyEvent.keyCode
    }))
  };
}

function gotMessageFromServer(message) {
  var signal = JSON.parse(message.data);
  if(signal.uuid === uuid) return;

  var targetPeerConnection = peerMap[signal.uuid];
  if(!targetPeerConnection) {
    // Init peer connection
    console.log("init peer connection ot " + signal.uuid);
    targetPeerConnection = new RTCPeerConnection(peerConnectionConfig);
    targetPeerConnection.onicecandidate = gotIceCandidate;
    stream.getTracks().forEach(function(track) {
      targetPeerConnection.addTrack(track, stream);
    });
    peerMap[signal.uuid] = targetPeerConnection;
    targetPeerConnection.addEventListener('signalingstatechange', () => console.log('Signaling state ' + targetPeerConnection.signalingState));
    targetPeerConnection.addEventListener('icegatheringstatechange', () => console.log('ICE gathering state ' + targetPeerConnection.iceGatheringState));
    targetPeerConnection.addEventListener('connectionstatechange', () => console.log('Connection state ' + targetPeerConnection.connectionState));
    targetPeerConnection.ondatachannel = receiveChannelCallback;
  }

  if(signal.sdp) {
    targetPeerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp)).then(function() {
      if(signal.sdp.type === 'offer') {
        targetPeerConnection.createAnswer().then((description) => {
          createdDescription(targetPeerConnection, description)
        }).catch(errorHandler);
      }
    }).catch(errorHandler);
  } else if(signal.ice) {
    targetPeerConnection.addIceCandidate(new RTCIceCandidate(signal.ice)).catch(errorHandler);
  }
}

function errorHandler(error) {
  console.log(error);
}

function createdDescription(peerConnection, description) {
  peerConnection.setLocalDescription(description).then(function() {
    serverConnection.send(JSON.stringify({'sdp': peerConnection.localDescription, 'uuid': uuid}));
  }).catch(errorHandler);
}

pageReady();